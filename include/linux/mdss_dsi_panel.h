/* Copyright (c) 2014 cocafe <cocafehj@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#ifndef _MDSS_DSI_PANEL_H
#define _MDSS_DSI_PANEL_H

#define DISPLAY_ON		1
#define DISPLAY_OFF		0

int mdss_panel_status(void);

#endif /* __MDSS_DSI_PANEL_H__ */
